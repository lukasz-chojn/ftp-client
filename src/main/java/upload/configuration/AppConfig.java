package upload.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Klasa konfiguracyjna aplikacji
 */
@Configuration
@EnableWebMvc
public class AppConfig implements WebMvcConfigurer {

    private final Logger logger = LoggerFactory.getLogger(AppConfig.class);

    @Bean
    public ViewResolver viewResolver() {
        if (logger.isInfoEnabled()) {
            logger.info(String.format("Konfiguracja bean'a %s", ViewResolver.class.getSimpleName()));
        }
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setSuffix(".html");
        viewResolver.setCache(false);
        return viewResolver;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (logger.isInfoEnabled()) {
            logger.info("Konfiguracja danych statycznych");
        }
        registry.addResourceHandler("/resources/**").addResourceLocations("resources/");
        registry.addResourceHandler("/**").addResourceLocations("classpath:static/");
        registry.addResourceHandler("/css/**").addResourceLocations("classpath:static/css/");
        registry.addResourceHandler("/js/**").addResourceLocations("classpath:static/js/");
        registry.addResourceHandler("/img/**").addResourceLocations("classpath:static/img/");
    }
}
