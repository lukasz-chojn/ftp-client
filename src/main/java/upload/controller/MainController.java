package upload.controller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Główny kontroler aplikacji
 */
@RestController
@RequestMapping("/files")
public class MainController {

    private final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Value("${ftp.url}")
    private String ftpAddress;
    @Value("${ftp.user}")
    private String login;
    @Value("${ftp.pass}")
    private String password;

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile multipartFile) throws IOException {

        if (Objects.equals(multipartFile.getOriginalFilename(), "")) {
            LOGGER.error("Filename is empty");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Filename is empty");
        } else {

            FTPClient ftpClient = new FTPClient();
            setupConnection(ftpClient);

            LOGGER.info("Sending file");
            ftpClient.storeFile(multipartFile.getOriginalFilename(), multipartFile.getInputStream());

            closeConnection(ftpClient);

            LOGGER.info(String.format("File %s uploaded with Content Type %s", multipartFile.getOriginalFilename(), multipartFile.getContentType()));
            return ResponseEntity.status(HttpStatus.OK).body("File " + multipartFile.getOriginalFilename() + " uploaded" + " with content type " + multipartFile.getContentType());
        }
    }

    @GetMapping(value = "/display", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String[]> displayFile() throws IOException {
        FTPClient ftpClient = new FTPClient();
        setupConnection(ftpClient);
        LOGGER.info("Obtaining files list");
        Map<String, String[]> ftpFiles = new HashMap<>();
        for (FTPFile file : ftpClient.listFiles()) {
            if (!file.isDirectory()) {
                LOGGER.info(String.format("File on server %s. Filesize is %s", file.getName(), FileUtils.byteCountToDisplaySize(file.getSize())));
                ftpFiles.put(file.getName(), new String[]{FileUtils.byteCountToDisplaySize(file.getSize()), String.valueOf(file.getTimestamp().getTime())});
            }
        }
        if (ftpFiles.isEmpty()) {
            LOGGER.info("No files found on FTP server");
            return Collections.emptyMap();
        }
        closeConnection(ftpClient);
        return ftpFiles;
    }

    private void setupConnection(FTPClient ftpClient) throws IOException {
        LOGGER.info("Trying to obtain connection");
        ftpClient.connect(ftpAddress);
        ftpClient.login(login, password);
        ftpClient.enterLocalPassiveMode();
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        LOGGER.info("Connection established");
    }

    private void closeConnection(FTPClient ftpClient) throws IOException {
        LOGGER.info("Trying to close connection");
        ftpClient.logout();
        ftpClient.disconnect();
        LOGGER.info("Connection closed");
    }

}
