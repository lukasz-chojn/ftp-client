package upload.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.io.IOException;
import java.net.SocketException;

/**
 * Klasa przechwytująca błędy aplikacji
 */
@RestControllerAdvice
public class ExceptionsHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(ExceptionsHandler.class);
    private final String CONTENT_TYPE = "Content-Type";
    private final String CONTENT_TYPE_VALUE = "text/html; charset=UTF-8";

    public ExceptionsHandler() {
        LOGGER.info(String.format("Podnoszę RestControllerAdvice %s", ExceptionsHandler.class.getSimpleName()));
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> bladPrzesylaniaDanychException(Exception e) {
        LOGGER.error("Wystąpił problem z przesyłaniem pliku." +
                " Sprawdź czy wszystko jest w porządku z połączeniem z internetem.");
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                .body("Wystąpił problem z przesyłaniem pliku." +
                        " Sprawdź czy wszystko jest w porządku z połączeniem z internetem.");
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<String> zaDuzyPlikException(Exception e) {
        LOGGER.error("Wielkość pliku jest zbyt duża. Maksymalny rozmiar to 10 240 KB");
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                .body("Wielkość pliku jest zbyt duża. Maksymalny rozmiar to 10 240 KB");
    }

    @ExceptionHandler(SocketException.class)
    public ResponseEntity<String> resetPolaczenia(Exception e) {
        LOGGER.error("Połączenie zostało zresetowane. Sprawdź połączenie oraz stan serwera");
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                .body("Połączenie zostało zresetowane. Sprawdź połączenie oraz stan serwera");
    }
}
