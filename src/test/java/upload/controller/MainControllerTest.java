package upload.controller;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.FileSystem;
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MainControllerTest {

    private MockMvc mockMvc;
    private MockMultipartFile multipartFile;
    private FakeFtpServer fakeFtpServer;
    private FTPClient ftpClient;

    @BeforeMethod
    public void setUp() throws IOException {
        MainController mainController = new MainController();
        mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
        multipartFile = new MockMultipartFile("file",
                "testFile.txt", MediaType.TEXT_PLAIN_VALUE, "test".getBytes());

        fakeFtpServer = new FakeFtpServer();
        fakeFtpServer.addUserAccount(new UserAccount("user", "password", "/data"));

        FileSystem fileSystem = new UnixFakeFileSystem();
        fileSystem.add(new DirectoryEntry("/data"));
        fileSystem.add(new FileEntry("/data/foobar.txt", "abcdef 1234567890"));
        fakeFtpServer.setFileSystem(fileSystem);
        fakeFtpServer.setServerControlPort(0);

        fakeFtpServer.start();

        ftpClient = new FTPClient();
        ftpClient.connect("localhost", fakeFtpServer.getServerControlPort());
        ftpClient.login("user", "password");
        ftpClient.enterLocalPassiveMode();
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
    }

    @Test
    public void testUploadFile() throws Exception {
        ResultActions resultActions = mockMvc.perform
                (
                multipart("/files/upload/")
                .file(multipartFile)
        );

        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
        assertEquals(resultActions.andReturn().getResponse().getContentLength(), 55);
        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.TEXT_PLAIN_VALUE + ";charset=ISO-8859-1");
    }

    @Test
    public void testDisplayFile() throws IOException {
        FTPFile[] files = ftpClient.listFiles();

        assertTrue(files.length != 0);
    }

    @AfterMethod
    public void teardown() throws IOException {
        ftpClient.logout();
        ftpClient.disconnect();
        fakeFtpServer.stop();
    }
}